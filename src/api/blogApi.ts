import axios, { AxiosHeaders } from "axios";

const blogApi = axios.create({
    baseURL: process.env.API_URL,
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
});

blogApi.interceptors.request.use(config => {
    const token = localStorage.getItem("token");
    if (token) {
        (config.headers as AxiosHeaders).set('x-token', `${token}`)
    }
    // config.headers = {
    //     ...config.headers,  
    //     'x-token':localStorage.getItem("token")
    // }
    return config;
});

export default blogApi;