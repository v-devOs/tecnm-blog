import React, { useState } from 'react'
import { LoginForm } from './ui/LoginForm';

const LoginPage = () => {

    return (
        <section id='login-background' className='flex justify-content align-center gap-50'>

            <img className='image-login' src="https://celaya.tecnm.mx/wp-content/uploads/2021/02/cropped-FAV.png" alt="logo itc" />

            <div className='flex column align-center'>
                <div className='white-border radius' style={{ minWidth: 400, backgroundColor: 'white', padding: 48 }}>
                    <h1>Iniciar sesión</h1>
                    <LoginForm />
                </div>

                {/* <div className='mt-10'>
                <span className='bold f-size-12'>Copyright @ITC 2024 | </span><span className='bold f-size-12'>Contáctanos</span>
                </div> */}

            </div>

            <img className='image-login' src="https://assets-global.website-files.com/6364b6fd26e298b11fb9391f/6364b6fd26e2989603b93c66_631843eb4387fd51e147b2fc_DrawKit0031_People_%2526_Technology_Thumbnail.png" alt="Chicos posteando" />

        </section>
    )
}

export default LoginPage