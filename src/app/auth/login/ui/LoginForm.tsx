'use client';

import { useAuthStore } from '@/hooks';
import React, { useState } from 'react'
import { useForm } from 'useform-simple-hook';
import { ShowIcon } from './ShowIcon'

interface showPass {
    show: boolean
    inp: number
}

const loginForm = {
    loginEmail: "",
    loginPassword: "",
};

interface LoginForm {
    loginEmail?: string;
    loginPassword?: string;
}

export const LoginForm = () => {

    const [showPass, setShowPass] = useState<showPass>({show: false, inp: 0});

    const showPassword = (show: showPass) => {
        setShowPass(show);
    }

    const { startLogin } = useAuthStore();

    const {
        formState, onInputChange } = useForm(loginForm);

    const { loginEmail, loginPassword }: LoginForm = formState;

    const onLoginSubmit = (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        if (!loginEmail || !loginPassword) return alert('Por favor ingrese su correo y contraseña');
        startLogin({ email: loginEmail, password: loginPassword });
    };

    return (
        <form onSubmit={onLoginSubmit} className='flex column'>
            <input type="text" className='input' name='loginEmail' value={loginEmail} onChange={onInputChange} />

            <span  className='absolute mt-50'>
                <input type={(showPass.show && showPass.inp==1) 
                        ? 'text' : 'password' }
                        className='relative input' name='loginPassword' style={{ minWidth: 300}} value={loginPassword} 
                        onChange={onInputChange} />

                { (showPass.inp==1) ? 
                      <ShowIcon show={showPass.show} inp={1} showPassword={showPassword}/> 
                        : 
                          <ShowIcon show={false} inp={1} showPassword={showPassword}/>
                }

            </span>

            <button type='submit' className='p-10 radius white-border black-text' 
            style={{marginTop: 70, backgroundColor: '#fec785'}}>Ingresar</button>


        </form>
    )
}
