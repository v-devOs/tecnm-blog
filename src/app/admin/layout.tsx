import Link from "next/link"

export default function RootLayout({
    children,
}: {
    children: React.ReactNode
}) {
    return (
        <div className='dashboard'>
            <div className='bg-secondary p-20 m-0 flex column justify-content' style={{ borderRadius: '0 10px 10px 0' }}>
                <h3 className='mb-30 title-sidebar'>Tecnm Blogs</h3>
                <div className='flex column'>
                    <Link href='/admin/usuarios' className='mb-25 cursor-pointer'>
                        <i className='fa-regular fa-user'></i>
                        Usuarios
                    </Link>
                    <Link href='/admin/articulos' className='mb-25 cursor-pointer'>
                        <i className="fa-regular fa-newspaper"></i>
                        Blogs
                    </Link>
                </div>
            </div>
            <div className=' p-20 '>
                {children}
            </div>
        </div>
    )
}
