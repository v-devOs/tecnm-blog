const ArticulosPage = () => {

    return (
        <>
            <div className='content flex gap-25 mb-30 m-0'>
                <h1>Blogs Posteados</h1>

                <ul className='menu-content flex gap-15'>
                    {/* TODO: PARA QUE ESTO FUNCIONE, EN EL URL HAY QUE MANDAR UN QUERY */}
                    <li className='cursor-pointer'>Publicados</li>
                    <li className='cursor-pointer'>Borradores</li>
                    <li className='cursor-pointer'>Eliminados</li>
                    <li className='cursor-pointer'>Todos</li>
                </ul>
            </div>
            <section className='grid-c-4 gap-15'>
                <div className='bg-secondary p-10 radius'>
                    <div className='mb-10'>
                        <h3>Algun titulo del blog</h3>
                    </div>
                    <div className='mb-50'>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo tempore assumenda dignissimos, consectetur laborum odio.
                        </p>
                    </div>
                    <div className='flex space-between f-size-14'>
                        <p>Uriel Emiliano Galindo Lopez</p>
                        <p>2024-12-01</p>
                    </div>
                </div>
                <div className='bg-secondary p-10 radius'>
                    <div className='mb-10'>
                        <h3>Algun titulo del blog</h3>
                    </div>
                    <div className='mb-50'>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo tempore assumenda dignissimos, consectetur laborum odio.
                        </p>
                    </div>
                    <div className='flex space-between f-size-14'>
                        <p>Uriel Emiliano Galindo Lopez</p>
                        <p>2024-12-01</p>
                    </div>
                </div>
                <div className='bg-secondary p-10 radius'>
                    <div className='mb-10'>
                        <h3>Algun titulo del blog</h3>
                    </div>
                    <div className='mb-50'>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo tempore assumenda dignissimos, consectetur laborum odio.
                        </p>
                    </div>
                    <div className='flex space-between f-size-14'>
                        <p>Uriel Emiliano Galindo Lopez</p>
                        <p>2024-12-01</p>
                    </div>
                </div>
                <div className='bg-secondary p-10 radius'>
                    <div className='mb-10'>
                        <h3>Algun titulo del blog</h3>
                    </div>
                    <div className='mb-50'>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo tempore assumenda dignissimos, consectetur laborum odio.
                        </p>
                    </div>
                    <div className='flex space-between f-size-14'>
                        <p>Uriel Emiliano Galindo Lopez</p>
                        <p>2024-12-01</p>
                    </div>
                </div>
                <div className='bg-secondary p-10 radius'>
                    <div className='mb-10'>
                        <h3>Algun titulo del blog</h3>
                    </div>
                    <div className='mb-50'>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo tempore assumenda dignissimos, consectetur laborum odio.
                        </p>
                    </div>
                    <div className='flex space-between f-size-14'>
                        <p>Uriel Emiliano Galindo Lopez</p>
                        <p>2024-12-01</p>
                    </div>
                </div>
                <div className='bg-secondary p-10 radius'>
                    <div className='mb-10'>
                        <h3>Algun titulo del blog</h3>
                    </div>
                    <div className='mb-50'>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo tempore assumenda dignissimos, consectetur laborum odio.
                        </p>
                    </div>
                    <div className='flex space-between f-size-14'>
                        <p>Uriel Emiliano Galindo Lopez</p>
                        <p>2024-12-01</p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default ArticulosPage;