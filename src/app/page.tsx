import Image from "next/image";
import Link from "next/link";

export default function Home() {
  return (
    <main className="mh-100">
      <header className="grid-60-40 bg-secondary mt-20 mh-20">
        <div className="p-50">
          <p className="f-size-50 bold">Bienvenido a <span className="bold">TecBlog</span>, tus noticias de confianza.</p>
          <p className="f-size-22 mt-10">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur, consectetur suscipit delectus error excepturi totam qui fugiat veniam repellendus sequi praesentium.</p>
          <div className="mt-50 flex gap-25">
            <Link href='/contacto' className="bg-black white-text p-10 radius">Contacto</Link>
            <Link href='/blog' className="bg-transparent black-border black-text p-10 radius">Ver todos los artículos</Link>
          </div>
        </div>
        <div className="flex justify-content align-center">
          <Image src='/resources/ilustrations/blog.png' alt="" width={1000} height={1000} className="max-width" />
        </div>
      </header>
      <section className="mt-50 mh-40">
        <h2>Nuestros favoritos</h2>
        <hr className="mt-20" />
        <div className="mt-20 grid-c-3 gap-25">
          <div>
            <Image src='/resources/images/test.jpg' alt="" width={500} height={300} className="max-width" />
            <p className="f-size-24 mt-20 bold">Google lanza el futuro de los videojuegos</p>
            <p className="gray-color mt-10 gray-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos facilis, praesentium quaerat commodi.</p>
            <p className="bold mt-20 f-size-14">Luis leal</p>
            <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
          </div>
          <div className="flex column gap-25">
            <div className="grid-c-2 grid-c-2-mobile gap-25">
              <Image src='/resources/images/test-1.jpg' alt="" width={200} height={200} className="max-width" />
              <div>
                <p className="f-size-18 bold">Google lanza el futuro de los videojuegos</p>
                <p className="bold mt-20 f-size-14">Luis leal</p>
                <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
              </div>
            </div>
            <div className="grid-c-2 grid-c-2-mobile gap-25">
              <Image src='/resources/images/test-1.jpg' alt="" width={200} height={200} className="max-width" />
              <div>
                <p className="f-size-18 bold">Google lanza el futuro de los videojuegos</p>
                <p className="bold mt-20 f-size-14">Luis leal</p>
                <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
              </div>
            </div>
          </div>
          <div className="flex column gap-25">
            <div className="grid-30-70 gap-25">
              <Image src='/resources/images/test-1.jpg' alt="" width={200} height={200} className="max-width" />
              <div>
                <p className="f-size-18 bold">Google lanza el futuro de los videojuegos</p>
                <p className="bold mt-20 f-size-14">Luis leal</p>
                <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
              </div>
            </div>
            <div className="grid-30-70 gap-25">
              <Image src='/resources/images/test-1.jpg' alt="" width={200} height={200} className="max-width" />
              <div>
                <p className="f-size-18 bold">Google lanza el futuro de los videojuegos</p>
                <p className="bold mt-20 f-size-14">Luis leal</p>
                <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
              </div>
            </div>
            <div className="grid-30-70 gap-25">
              <Image src='/resources/images/test-1.jpg' alt="" width={200} height={200} className="max-width" />
              <div>
                <p className="f-size-18 bold">Google lanza el futuro de los videojuegos</p>
                <p className="bold mt-20 f-size-14">Luis leal</p>
                <p className="f-size-14 gray-text">Marzo 20, 2023.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}
