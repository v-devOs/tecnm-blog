import React from 'react'
import Letter from './ui/Letter'

const ContactoPage = () => {


    return (
        <section className='flex column total-width' style={{height: '105dvh'}}>
            <div className='bg-secondary total-width f-grow1 shadow-2' style={{zIndex: '1'}}>

                <div className='mh-40  mt-50'>
                    <span className='white-text bold f-size-50'>Contáctanos</span>
                    <br />
                    <span className='pt-50 white-text f-size-28'>¿Buscas un diseño atractivo?</span>
                    <br />
                    <span className='white-text f-size-28'>Lo has encontrado</span>
                </div>
            </div>

            <Letter/>

            <div className='bg-primary total-width f-grow3'>
                {/* <span>section 2</span> */}
            </div> 

        </section>
    )
}

export default ContactoPage